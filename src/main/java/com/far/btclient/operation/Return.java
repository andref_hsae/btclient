/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.far.btclient.operation;

/**
 *
 * @author andref
 */
public class Return {
    
    String status;
    
    public void setStatus(String s) {
        status = s;
    }
    
    public String getStatus() {
        return status;
    }
    
    String created;
    
    public void setCreated(String t) {
        created = t;
    }
    
    public String getCreated() {
        return created;
    }
    
    Double price;
    
    public void setPrice(String d) {
        price = Double.parseDouble(d);
    }
    
    public Double getPrice() {
        return price;
    }
    
    String volume;
    
    public void setVolume(String v) {
        volume = v;
    }
    
    public String getVolume() {
        return volume;
    }
    
    String pair;
    
    public void setPair(String p) {
        pair = p;
    }
    
    public String getPair() {
        return pair;
    }
    
    String type;
    
    public void setType(String t) {
        type = t;
    }
    
    public String getType() {
        return type;
    }
    
    Operations operations;
    
    public void setOperations(Operations op) {
        operations = op;
    }
    
    public Operations getOperations() {
        return operations;
    }
}
