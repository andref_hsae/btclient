/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.far.btclient.operation.GetInfo;

/**
 *
 * @author andref
 */
public class Info {
    
    public String success;
    
    public void setSuccess(String s) {
        this.success = s;
    }
    
    public String getSuccess() {
        return this.success;
    }
    
    public Return returndata;
    
    public void setReturn(Return r) {
        this.returndata = r;
    }
    
    public Return getReturn() {
        return this.returndata;
    }
}
