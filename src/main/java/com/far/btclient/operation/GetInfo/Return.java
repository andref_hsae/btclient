/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.far.btclient.operation.GetInfo;

/**
 *
 * @author andref
 */
public class Return {
    public Funds funds;
    
    public void setFunds(Funds f) {
        this.funds = f;
    }
    
    public Funds getFunds() {
        return this.funds;
    }
    
    public String server_time = "";
    
    public void setServerTime(String s) {
        this.server_time = s;
    }
    
    public String getServerTime() {
        return this.server_time;
    }
    
    public int open_orders = 0;
    
    public void setOpenOrders(int o) {
        this.open_orders = o;
    }
    
    public int getOpenOrders() {
        return this.open_orders;
    }
}
