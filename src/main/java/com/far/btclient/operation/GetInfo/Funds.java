/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.far.btclient.operation.GetInfo;

/**
 *
 * @author andref
 */
public class Funds {
    
    public Double ltc = 0.0;
    
    public void setLtc(Double l) {
        this.ltc = l;
    }
    
    public Double getLtc() {
        return this.ltc;
    }
    
    public Double brl = 0.0;
    
    public void setBrl(Double b) {
        this.brl = b;
    }
    
    public Double getBrl() {
        return this.brl;
    }
    
    public Double btc = 0.0;
    
    public void setBtc(Double b) {
        this.btc = b;
    }
    
    public Double getBtc() {
        return this.btc;
    }
}
