/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.far.btclient.operation;

/**
 *
 * @author andref
 */
public class CancelOrder {
    String order_id;
    
    public CancelOrder setOrderId(String i) {
        order_id = i;
        return this;
    }
    
    public String getOrderId() {
        return this.order_id;
    }
    
    CancelOrder(String id) {
        order_id = id;
    }
    
    CancelOrder(String id, String p) {
        order_id = id;
        pair = p;
    }
    
    String pair;
    
    public CancelOrder setPair(String p) {
        pair = p;
        return this;
    }
    
    public String getPair() {
        return pair;
    }
}
