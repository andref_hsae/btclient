/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.far.btclient.operation;

/**
 *     
 * success: indicador de sucesso da requisição. Retorna 1 se requisição bem sucedida.
 *  return: dados de retorno deste método: retorna o id e os dados da ordem cancelada.
 *   status: status da ordem: 'active' para ordens ainda ativas ou em aberto, 'canceled' para ordens que foram canceladas e 'completed' para ordens que foram completadas ou preenchidas.
 *   created: timestamp da criação da ordem.
 *   price: preço unitário em Reais de compra ou venda da ordem.
 *   volume: volume de compra ou venda de criptomoeda da ordem, seja Bitcoin ou Litecoin.
 *   pair: par da ordem: 'btc_brl' para ordens de compra ou venda de Bitcoins e 'ltc_brl' para ordens de compra e venda de Litecoins.
 *   type: tipo da ordem: 'buy' para ordens de compra e 'sell' para ordens de venda.
 *   operations: lista das operações que a ordem sofreu pelo id da operação. Acontecerá com ordens que sofreram alguma execução já em sua criação:
 *       volume: volume de criptomoeda executado nesta operação, seja Bitcoin ou Litecoin.
 *       price: preço unitário em Reais executado nesta operação.
 *       rate: taxa em percentual aplicada nesta operação.
 *       created: timestamp operaçao.
 * 
 * @author andref
 */
public class TradeAnswer {
  
    String success;
    
    public TradeAnswer setSuccess(String s) {
        success = s;
        return this;
    }
    
    public String getSuccess() {
        return success;
    }
    
    Return returnData;
    
    public void setReturnData(Return rl) {
        returnData = rl;
    }
    
    public Return getReturnData() {
        return returnData;
    }
}
