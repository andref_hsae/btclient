/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.far.btclient.operation;

/**
 *
 * @author andref
 */
public class Trade {
    /*
    *pair: par da ordem: 'btc_brl' para ordens de compra ou venda de Bitcoins e 
    'ltc_brl' para ordens de compra e venda de Litecoins.
    */
    String pair = null;
    
    public Trade setPair(String p) {
        pair = p;
        return this;
    }
    
    public String getPair() {
        return pair;
    }
    
    /**
     * *type: tipo da ordem: 'buy' para ordens de compra e 'sell' para ordens de venda.
     */
    String type = null;
    
    public Trade setType(String t) {
        type = t;
        return this;
    }
    
    public String getType() {
        return type;
    }
    /*
    *volume: volume de criptomoeda para compra ou venda, seja Bitcoin ou Litecoin.
    */
    String volume;
    
    public Trade setVolume(String v) {
        volume = v;
        return this;
    }
    
    public String getVolume() {
        return volume;
    }
    /*
    *price: preço unitário em Reais para compra ou venda.
    */
    Double price;
    
    public Trade setPrice(String p) {
        price = Double.valueOf(p);
        return this;
    }
    
    public String getPrice() {
        return price.toString();
    }
    
    public Double getPriceHasDouble() {
        return price;
    }
}
