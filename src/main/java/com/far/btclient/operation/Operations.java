/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.far.btclient.operation;

/**
 *
 * @author andref
 */
public class Operations {
    /*
    *volume: volume de criptomoeda para compra ou venda, seja Bitcoin ou Litecoin.
    */
    String volume;
    
    public void setVolume(String v) {
        volume = v;
    }
    
    public String getVolume() {
        return volume;
    }
    
    Double price;
    
    public void setPrice(String d) {
        price = Double.parseDouble(d);
    }
    
    public Double getPrice() {
        return price;
    }
    
    Double rate;
    
    public void setRate(String r) {
        rate = Double.parseDouble(r);
    }
    
    public Double getRate() {
        return rate;
    }
    
    String created;
    
    public void setCreated(String c) {
        created = c;
    }
    
    public String getCreated() {
        return created;
    }
}
