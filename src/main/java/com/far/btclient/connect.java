/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.far.btclient;

import java.io.IOException;
import com.far.btclient.communication.Info;
import com.far.btclient.communication.TradeOrder;
import java.util.HashMap;
import org.apache.http.client.methods.HttpPost;

/**
 *
 * @author andref
 */
public class connect {
    
    public connect() throws IOException
    {
        HashMap hm = new HashMap<String, String>();
        hm.put("X-KEY", "Key");
        hm.put("X-VAL", "Val");
        hm.put("X-SIGN-KEY", "t1");
        hm.put("X-SIGN-VAL", "v1");

        Info info;
        info = new Info(hm);
        new Thread(info).start();
        
        TradeOrder to = new TradeOrder(hm, "buy", "btc_brl", 0.03 , 0.03);
        new Thread(to).start();
        
    }
    
}
