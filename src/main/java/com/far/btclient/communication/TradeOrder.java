/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.far.btclient.communication;

import com.far.btclient.operation.Trade;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

/**
 *
 * @author andref
 */
public class TradeOrder implements Runnable {
    HttpPost request;
    Trade trade;
    
    /**
     * 
     * @param m
     * @param OrderType
     * @param pair
     * @param volume
     * @param price 
     */
    public TradeOrder(HashMap m, String OrderType, 
            String pair,
            Double volume,
            Double price
    ) {
        long unixTime = System.currentTimeMillis() / 1000L;
        String url = "http://192.168.0.5/getinfo/getinfo.html?method=Trade&tonce=" 
                + String.valueOf(unixTime);
        
        request = new HttpPost(url);
        
        request.addHeader(m.get("X-KEY").toString(), m.get("X-VAL").toString());
        request.addHeader(m.get("X-SIGN-KEY").toString(), m.get("X-SIGN-VAL").toString());
        request.setHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");

        trade = new Trade();
        trade.setPair(pair);
        trade.setType(OrderType);
        trade.setVolume(String.valueOf(volume));
        trade.setPrice(String.valueOf(price));
        
    }
        
    public void connect() {
        HttpClient client = new DefaultHttpClient();
        HttpResponse response = null;
        
        ObjectMapper mapper = new ObjectMapper();
        //mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
        //mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        
        try {
            //mapper.writeValueAsString(trade);
            // Request parameters and other properties.
            List<NameValuePair> params = new ArrayList<NameValuePair>(2);
            params.add(new BasicNameValuePair("pair", trade.getPair()));
            params.add(new BasicNameValuePair("type", trade.getType()));
            params.add(new BasicNameValuePair("volume", trade.getVolume()));
            params.add(new BasicNameValuePair("price", trade.getPrice()));
            request.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            
        } catch (IOException ex) {
            Logger.getLogger(Info.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            response = client.execute(request);
        } catch (IOException ex) {
            Logger.getLogger(Info.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        BufferedReader rd = null;
        try {
            rd = new BufferedReader (
                    new InputStreamReader(response.getEntity().getContent())
            );
        } catch (IOException ex) {
            Logger.getLogger(Info.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedOperationException ex) {
            Logger.getLogger(Info.class.getName()).log(Level.SEVERE, null, ex);
        }
        String line = "";
        StringBuilder responseStrBuilder = new StringBuilder();
        
        try {
            while ((line = rd.readLine()) != null) {
                System.out.println(line);
                responseStrBuilder.append(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(Info.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("completed post");
    }

    public void run() {
        this.connect();
    }
}
