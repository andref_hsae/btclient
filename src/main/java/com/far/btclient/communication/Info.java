/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.far.btclient.communication;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpHeaders;

import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import org.apache.http.client.methods.HttpPost;

/**
 *
 * @author andref
 */
public class Info implements Runnable {
    
    HttpPost request;
    
    public Info(HashMap m) {
        long unixTime = System.currentTimeMillis() / 1000L;
        String url = "http://192.168.0.5/getinfo/getinfo.html?method=getInfo&tonce=" 
                + String.valueOf(unixTime);
        
        request = new HttpPost(url);
        
        request.addHeader(m.get("X-KEY").toString(), m.get("X-VAL").toString());
        request.addHeader(m.get("X-SIGN-KEY").toString(), m.get("X-SIGN-VAL").toString());
        request.setHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
    }
        
    public void connect() {
        HttpClient client = new DefaultHttpClient();
        HttpResponse response = null;
        
        try {
            response = client.execute(request);
        } catch (IOException ex) {
            Logger.getLogger(Info.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        BufferedReader rd = null;
        try {
            rd = new BufferedReader (
                    new InputStreamReader(response.getEntity().getContent())
            );
        } catch (IOException ex) {
            Logger.getLogger(Info.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedOperationException ex) {
            Logger.getLogger(Info.class.getName()).log(Level.SEVERE, null, ex);
        }
        String line = "";
        StringBuilder responseStrBuilder = new StringBuilder();
        
        try {
            while ((line = rd.readLine()) != null) {
                System.out.println(line);
                responseStrBuilder.append(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(Info.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String resp = responseStrBuilder.toString().replace("'", "\"");
        //2. Convert JSON to Java object
        ObjectMapper mapper = new ObjectMapper();
        //mapper.configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
        //mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        com.far.btclient.operation.GetInfo.Info user=null;
        try {
            user = mapper.readValue(resp , com.far.btclient.operation.GetInfo.Info.class);
        } catch (IOException ex) {
            Logger.getLogger(Info.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println( user.getReturn().getFunds().getBrl() );
    }

    public void run() {
        this.connect();
    }
}
